package nl.michael.services;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;

import java.io.IOException;

public class ConfigService {
    private static ConfigService instance;

    private ConfigurationLoader<CommentedConfigurationNode> loader;
    private ConfigurationNode rootNode;

    private ConfigService(ConfigurationLoader<CommentedConfigurationNode> loader) {
        this.loader = loader;
        try {
            rootNode = loader.load(ConfigurationOptions.defaults());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setHubPosition(Vector3i position) {
        ConfigurationNode hubPosition = rootNode.getNode("hubPosition");
        TypeToken<Vector3i> token = new TypeToken<Vector3i>() {};
        try {
            hubPosition.setValue(token, position);
            save();
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    public Vector3i getHubPosition() {
        ConfigurationNode hubPosition = rootNode.getNode("hubPosition");

        if(hubPosition != null && !hubPosition.isVirtual()) {
            int x = (int) hubPosition.getNode("x").getValue();
            int y = (int) hubPosition.getNode("y").getValue();
            int z = (int) hubPosition.getNode("z").getValue();

            return new Vector3i(x, y, z);
        } else {
            return null;
        }

        /*HashMap<String, Double> positionMap = (HashMap<String, Double>) hubPosition.getValue();

        return new Vector3d(positionMap.get("x"), positionMap.get("y"), positionMap.get("z"));*/
    }

    public void setTeamRedPosition(Vector3i position) {
        ConfigurationNode teamRedPosition = rootNode.getNode("teamRedPosition");
        TypeToken<Vector3i> token = new TypeToken<Vector3i>() {};
        try {
            teamRedPosition.setValue(token, position);
            save();
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    public void setTeamBluePosition(Vector3i position) {
        ConfigurationNode teamBluePosition = rootNode.getNode("teamBluePosition");
        TypeToken<Vector3i> token = new TypeToken<Vector3i>() {};
        try {
            teamBluePosition.setValue(token, position);
            save();
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    public Vector3i getTeamRedPosition() {
        ConfigurationNode teamRedPosition = rootNode.getNode("teamRedPosition");

        if(teamRedPosition != null && !teamRedPosition.isVirtual()) {
            int x = (int) teamRedPosition.getNode("x").getValue();
            int y = (int) teamRedPosition.getNode("y").getValue();
            int z = (int) teamRedPosition.getNode("z").getValue();

            return new Vector3i(x, y, z);
        } else {
            return Vector3i.ONE;
        }
    }

    public Vector3i getTeamBluePosition() {
        ConfigurationNode teamBluePosition = rootNode.getNode("teamBluePosition");

        if(teamBluePosition != null && !teamBluePosition.isVirtual()) {
            int x = (int) teamBluePosition.getNode("x").getValue();
            int y = (int) teamBluePosition.getNode("y").getValue();
            int z = (int) teamBluePosition.getNode("z").getValue();

            return new Vector3i(x, y, z);
        } else {
            return Vector3i.ONE;
        }
    }

    public void unsetRedBombPosition() {
        ConfigurationNode redBombPosition = rootNode.getNode("redBombPosition");
        redBombPosition.setValue(null);

        save();
    }

    public void unsetBlueBombPosition() {
        ConfigurationNode blueBombPosition = rootNode.getNode("blueBombPosition");
        blueBombPosition.setValue(null);

        save();
    }

    public void setRedBombPosition(Vector3i position) {
        ConfigurationNode redBombPosition = rootNode.getNode("redBombPosition");
        TypeToken<Vector3i> token = new TypeToken<Vector3i>() {};
        try {
            redBombPosition.setValue(token, position);
            save();
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    public Vector3i getBlueBombPosition() {
        ConfigurationNode blueBombPosition = rootNode.getNode("blueBombPosition");
        if(blueBombPosition != null && !blueBombPosition.isVirtual()) {
            int x = (int) blueBombPosition.getNode("x").getValue();
            int y = (int) blueBombPosition.getNode("y").getValue();
            int z = (int) blueBombPosition.getNode("z").getValue();

            return new Vector3i(x, y, z);
        } else {
            return null;
        }
    }

    public Vector3i getRedBombPosition() {
        ConfigurationNode redBombPosition = rootNode.getNode("redBombPosition");
        if(redBombPosition != null && !redBombPosition.isVirtual()) {
            int x = (int) redBombPosition.getNode("x").getValue();
            int y = (int) redBombPosition.getNode("y").getValue();
            int z = (int) redBombPosition.getNode("z").getValue();

            return new Vector3i(x, y, z);
        } else {
            return null;
        }
    }

    public void setBlueBombPosition(Vector3i position) {
        ConfigurationNode blueBombPosition = rootNode.getNode("blueBombPosition");
        TypeToken<Vector3i> token = new TypeToken<Vector3i>() {};
        try {
            blueBombPosition.setValue(token, position);
            save();
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    private void save() {
        try {
            loader.save(rootNode);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isConfigured() {
        return (
                this.getBlueBombPosition() != null &&
                        this.getRedBombPosition() != null &&
                        this.getHubPosition() != null &&
                        this.getTeamBluePosition() != null &&
                        this.getTeamRedPosition() != null
                );
    }

    public static void instantiate(ConfigurationLoader<CommentedConfigurationNode> loader) {
        if(instance == null) instance = new ConfigService(loader);
    }

    public static ConfigService getInstance() {
        return instance;
    }
}
