package nl.michael.bomb;

import java.util.ArrayList;
import java.util.List;

public class BombDetonateEvent {
    private static List<BombDetonateEventHandler> eventHandlers = new ArrayList<>();

    public static void registerEventHandler(BombDetonateEventHandler handler) {
        eventHandlers.add(handler);
    }

    protected static void fireDetonateEvent(TeamBomb bombInfo) {
        for (BombDetonateEventHandler handler: eventHandlers) {
            handler.handle(bombInfo);
        }
    }
}
