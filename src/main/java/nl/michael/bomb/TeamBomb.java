package nl.michael.bomb;

import nl.michael.teams.Team;

public class TeamBomb {
    private Team team;
    private double fuseState = 0;

    public TeamBomb(Team team) {
        this.team = team;
    }

    public void setFuseState(double value) {
        this.fuseState = value;

        // TODO: Check fuse value
    }

    public double getFuseState() { return fuseState; }

    public Team getTeam() { return team; }
}
