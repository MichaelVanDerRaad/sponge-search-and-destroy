package nl.michael.bomb;

public interface BombDetonateEventHandler {
    void handle (TeamBomb bombInfo);
}
