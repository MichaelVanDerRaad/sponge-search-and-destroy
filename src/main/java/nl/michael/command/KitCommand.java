package nl.michael.command;

import nl.michael.kits.IKit;
import nl.michael.kits.KitFactory;
import nl.michael.kits.KitType;
import nl.michael.plugin.MatchManager;
import nl.michael.plugin.PlayerData;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.permission.PermissionService;
import org.spongepowered.api.text.Text;

public class KitCommand extends PlayerCommand{
    public KitCommand(Object plugin) {
        super(plugin);
    }

    @Override
    public CommandResult execute(Player player, CommandContext args) {
        String kitName = args.<String>getOne("kit").get();
        PlayerData data = MatchManager.getInstance().getPlayerData(player.getUniqueId());

        if(MatchManager.getInstance().isGameStarted()) {
            player.sendMessage(Text.of("You can't change kit while playing"));
            return CommandResult.empty();
        }

        if(data == null) {
            player.sendMessage(Text.of("First select a team"));
            return CommandResult.empty();
        }

        String kitNameUpperCase = kitName.toUpperCase();
        KitType kitType;
        try {
            kitType = KitType.valueOf(kitNameUpperCase);
        } catch (IllegalArgumentException e) {
            kitType = null;
        }

        if(kitType != null) {
            IKit kitInstance = KitFactory.getKitInstance(kitType);
            data.setKit(kitInstance);
            player.sendMessage(Text.of("Kit " + kitName + " is now selected!"));
            return CommandResult.success();
        } else {
            player.sendMessage(Text.of("This kit does not exist"));
            return CommandResult.empty();
        }
    }

    @Override
    public void registerCommand() {
        CommandSpec commandSpec = CommandSpec.builder()
                .description(Text.of("Choose a kit."))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("kit")))
                )
                .executor(this)
                .build();
        Sponge.getCommandManager().register(plugin, commandSpec, "kit");
    }

    @Override
    public void registerPermissions(PermissionService permissionService) {
        // No permission needed
    }

    @Override
    public String getPermissionName() {
        return "searchanddestroy.commands.kit.execute";
    }
}
