package nl.michael.command;

import nl.michael.services.ConfigService;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.service.permission.PermissionService;
import org.spongepowered.api.text.Text;

public class HubConfigCommand extends PlayerCommand {

    public HubConfigCommand(Object plugin) {
        super(plugin);
    }

    @Override
    public void registerCommand() {
        CommandSpec commandSpec = CommandSpec.builder()
                .description(Text.of("Set hub location"))
                .executor(this)
                .build();
        Sponge.getCommandManager().register(plugin, commandSpec, "sethub");
    }

    @Override
    public void registerPermissions(PermissionService permissionService) {
        PermissionDescription.Builder builder = permissionService.newDescriptionBuilder(plugin);
        builder.id(getPermissionName())
                .description(Text.of("Allows user to configure hub location."))
                .assign(PermissionDescription.ROLE_ADMIN, true)
                .register();
    }

    @Override
    public String getPermissionName() {
        return "searchanddestroy.commands.sethub.execute";
    }

    @Override
    public CommandResult execute(Player player, CommandContext args) {
        ConfigService.getInstance().setHubPosition(player.getLocation().getPosition().toInt());
        player.sendMessage(Text.of("Hub location set"));

        return CommandResult.success();
    }
}
