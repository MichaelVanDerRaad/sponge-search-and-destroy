package nl.michael.command;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public abstract class Command implements CommandExecutor, CommandRegister {
    protected Object plugin;

    public Command (Object plugin) {
        this.plugin = plugin;
    }

    @Override
    public CommandResult execute (CommandSource src, CommandContext args) throws CommandException {
        if (!src.hasPermission(getPermissionName())) {
            src.sendMessage(Text.of("You don't have permission to execute this command."));
            return CommandResult.empty();
        } else {
            return executeCommand(src, args);
        }
    }

    public abstract CommandResult executeCommand (CommandSource src, CommandContext args);
}
