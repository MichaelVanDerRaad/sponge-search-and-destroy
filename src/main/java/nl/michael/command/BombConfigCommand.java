package nl.michael.command;

import nl.michael.events.BlockPlaceListener;
import nl.michael.plugin.SearchAndDestroy;
import nl.michael.teams.ITeam;
import nl.michael.teams.Team;
import nl.michael.teams.TeamFactory;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.service.permission.PermissionService;
import org.spongepowered.api.text.Text;

public class BombConfigCommand extends PlayerCommand {

    public BombConfigCommand(Object plugin) {
        super(plugin);
    }

    @Override
    public void registerCommand() {
        CommandSpec commandSpec = CommandSpec.builder()
                .description(Text.of("Configure bomb location of a specific team"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("team")))
                )
                .executor(this)
                .build();

        Sponge.getCommandManager().register(plugin, commandSpec, "setbomb");
    }

    @Override
    public void registerPermissions(PermissionService permissionService) {
        PermissionDescription.Builder builder = permissionService.newDescriptionBuilder(plugin);
        builder.id(getPermissionName())
                .description(Text.of("Allows the user to configure the bomb locations."))
                .assign(PermissionDescription.ROLE_ADMIN, true)
                .register();
    }

    @Override
    public String getPermissionName() {
        return "searchanddestroy.commands.setbomb.execute";
    }

    @Override
    public CommandResult execute(Player player, CommandContext args) {
        String teamName = args.<String>getOne("team").get();
        String teamNameUpperCase = teamName.toUpperCase();
        ITeam teamInstance;

        try {
            Team teamType = Team.valueOf(teamNameUpperCase);
            teamInstance = TeamFactory.getTeamInstance(teamType);
        } catch (IllegalArgumentException e) {
            teamInstance = null;
        }

        if(teamInstance == null) {
            player.sendMessage(Text.of("This team doesn't exist"));
        } else {
            player.sendMessage(Text.of("Place bomb for team " + teamInstance.getType().toString()));

            player.getInventory().clear();
            player.setItemInHand(HandTypes.MAIN_HAND, ItemStack.of(ItemTypes.TNT, 1));

            Sponge.getEventManager().registerListener(SearchAndDestroy.getPlugin(), ChangeBlockEvent.Place.class, new BlockPlaceListener(teamInstance));
        }

        return CommandResult.success();
    }
}
