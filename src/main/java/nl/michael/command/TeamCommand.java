package nl.michael.command;

import nl.michael.plugin.MatchManager;
import nl.michael.plugin.SearchAndDestroy;
import nl.michael.teams.ITeam;
import nl.michael.teams.Team;
import nl.michael.teams.TeamFactory;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.permission.PermissionService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class TeamCommand extends PlayerCommand {
    public TeamCommand(Object plugin) {
        super(plugin);
    }

    @Override
    public CommandResult execute(Player player, CommandContext args) {
        if(MatchManager.getInstance().isGameStarted()) {
            player.sendMessage(Text.of("The game is already started"));
            return CommandResult.empty();
        }

        String teamName = args.<String>getOne(Text.of("team")).get();
        String teamNameUpperCase = teamName.toUpperCase();
        ITeam teamInstance;

        try {
            Team teamType = Team.valueOf(teamNameUpperCase);
            teamInstance = TeamFactory.getTeamInstance(teamType);
        } catch (IllegalArgumentException e) {
            teamInstance = null;
        }

        if(teamInstance == null) {
            player.sendMessage(Text.of("This team does not exist"));
        } else {
            MatchManager.getInstance().addPlayerToTeam(player, teamInstance);
            float teamBalance = MatchManager.getInstance().calculateTeamBalance();
            SearchAndDestroy.getPlugin().getLogger().info("Team balance: " + teamBalance);
            if (teamBalance >= 1f/3 && teamBalance <= 2f/3)
            {
                player.sendMessage(Text.of("You are now in team " + teamInstance.getType().toString()));
            } else {
                // Out of balance
                MatchManager.getInstance().removePlayerFromMatch(player);
                player.sendMessage(Text.builder("This team is full!").color(TextColors.RED).build());
            }
        }

        return CommandResult.success();
    }

    @Override
    public void registerCommand() {
        CommandSpec commandSpec = CommandSpec.builder()
                .description(Text.of("Choose a team."))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("team")))
                )
                .executor(this)
                .build();
        Sponge.getCommandManager().register(plugin, commandSpec, "team");
    }

    @Override
    public void registerPermissions(PermissionService permissionService) {
        permissionService.newDescriptionBuilder(plugin)
                .id(getPermissionName())
                .description(Text.of("Allows player to choose a team."))
                .register();
    }

    @Override
    public String getPermissionName() {
        return "searchanddestroy.commands.team.execute";
    }
}
