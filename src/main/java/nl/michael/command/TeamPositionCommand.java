package nl.michael.command;

import nl.michael.services.ConfigService;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.service.permission.PermissionService;
import org.spongepowered.api.text.Text;

public class TeamPositionCommand extends PlayerCommand {
    public TeamPositionCommand(Object plugin) {
        super(plugin);
    }

    @Override
    public void registerCommand() {
        CommandSpec commandSpec = CommandSpec.builder()
                .description(Text.of("Configure team spawn position"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("team")))
                )
                .executor(this)
                .build();
        Sponge.getCommandManager().register(plugin, commandSpec, "teamspawn");
    }

    @Override
    public void registerPermissions(PermissionService permissionService) {
        permissionService.newDescriptionBuilder(plugin)
                .description(Text.of("Allows the user to configure the teamspawn locations."))
                .id(getPermissionName())
                .assign(PermissionDescription.ROLE_ADMIN, true)
                .register();
    }

    @Override
    public String getPermissionName() {
        return "searchanddestroy.commands.teamspawn.execute";
    }

    @Override
    public CommandResult execute(Player player, CommandContext args) {
        String team = args.<String>getOne("team").get();

        switch (team) {
            case "blue":
                ConfigService.getInstance().setTeamBluePosition(player.getPosition().toInt());
                break;
            case "red":
                ConfigService.getInstance().setTeamRedPosition(player.getPosition().toInt());
                break;

            default:
                player.sendMessage(Text.of("This team does not exist"));
                return CommandResult.empty();
        }

        player.sendMessage(Text.of("Position set for team " + team));

        return CommandResult.success();
    }
}
