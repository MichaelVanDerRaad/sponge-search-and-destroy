package nl.michael.command;

import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public abstract class PlayerCommand implements CommandExecutor, CommandRegister {
    protected Object plugin;

    public PlayerCommand (Object plugin) {
        this.plugin = plugin;
    }

    @Override
    public final CommandResult execute (CommandSource src, CommandContext args) {
        if (!(src instanceof Player)) {
            src.sendMessage(Text.of("This command can only be executed in-game."));
            return CommandResult.empty();
        } else if (!src.hasPermission(getPermissionName())) {
            src.sendMessage(Text.of("You don't have permission to execute this command."));
            return CommandResult.empty();
        } else {
            return this.execute((Player) src, args);
        }
    }

    public abstract CommandResult execute (Player player, CommandContext args);
}
