package nl.michael.command;

import org.spongepowered.api.service.permission.PermissionService;

public interface CommandRegister {
    void registerCommand();
    void registerPermissions(PermissionService permissionService);
    String getPermissionName();
}
