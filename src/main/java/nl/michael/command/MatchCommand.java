package nl.michael.command;

import nl.michael.plugin.MatchManager;
import nl.michael.services.ConfigService;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.service.permission.PermissionDescription;
import org.spongepowered.api.service.permission.PermissionService;
import org.spongepowered.api.text.Text;

public class MatchCommand extends Command {

    public MatchCommand(Object plugin) {
        super(plugin);
    }

    @Override
    public void registerCommand() {
        CommandSpec matchCommandSpec = CommandSpec.builder()
                .description(Text.of("Start or stop a match"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.string(Text.of("option")))
                )
                .executor(this)
                .build();
        Sponge.getCommandManager().register(plugin, matchCommandSpec, "match");
    }

    @Override
    public void registerPermissions(PermissionService permissionService) {
        permissionService.newDescriptionBuilder(plugin)
                .id(getPermissionName())
                .description(Text.of("Allows the user to start or stop a match manually"))
                .assign(PermissionDescription.ROLE_ADMIN, true)
                .register();
    }

    @Override
    public String getPermissionName() {
        return "searchanddestroy.commands.match.execute";
    }

    @Override
    public CommandResult executeCommand(CommandSource src, CommandContext args) {
        String option = args.<String>getOne("option").get();

        switch (option) {
            case "start":
                if (ConfigService.getInstance().isConfigured()) {
                    MatchManager.getInstance().startMatch();
                } else {
                    src.sendMessage(Text.of("This plugin is not fully configured"));
                    return CommandResult.empty();
                }
                break;

            case "stop":
                MatchManager.getInstance().stopMatch();
                break;

            default:
                src.sendMessage(Text.of("Invalid option, choose: start/stop"));
                return CommandResult.empty();
        }

        return CommandResult.success();
    }
}
