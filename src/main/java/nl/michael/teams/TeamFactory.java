package nl.michael.teams;

public class TeamFactory {
    public static ITeam getTeamInstance(Team team) {
        switch (team) {
            case RED:
                return new RedTeam();
            case BLUE:
                return new BlueTeam();
            default:
                return null;
        }
    }
}
