package nl.michael.teams;

import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.entity.living.player.Player;

public interface ITeam {
    void teleportPlayer(Player player);

    void setBombPosition(Vector3i position);

    Team getType();
}
