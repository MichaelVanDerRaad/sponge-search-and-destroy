package nl.michael.teams;

import com.flowpowered.math.vector.Vector3i;
import nl.michael.services.ConfigService;
import org.spongepowered.api.entity.living.player.Player;

public class BlueTeam implements ITeam {
    @Override
    public void teleportPlayer(Player player) {
        player.setTransform(player.getTransform().setPosition(ConfigService.getInstance().getTeamBluePosition().toDouble()));
    }

    @Override
    public void setBombPosition(Vector3i position) {
        ConfigService.getInstance().setBlueBombPosition(position);
    }

    @Override
    public Team getType() {
        return Team.BLUE;
    }
}
