package nl.michael.teams;

import com.flowpowered.math.vector.Vector3i;
import nl.michael.services.ConfigService;
import org.spongepowered.api.entity.living.player.Player;

public class RedTeam implements ITeam {
    @Override
    public void teleportPlayer(Player player) {
        player.setTransform(player.getTransform().setPosition(ConfigService.getInstance().getTeamRedPosition().toDouble()));
    }

    @Override
    public void setBombPosition(Vector3i position) {
        ConfigService.getInstance().setRedBombPosition(position);
    }

    @Override
    public Team getType() {
        return Team.RED;
    }
}
