package nl.michael.teams;

public enum Team {
    BLUE {
        @Override
        public String getLabel() {
            return "Blue";
        }
    },
    RED {
        @Override
        public String getLabel() {
            return "Red";
        }
    };

    public abstract String getLabel();
}
