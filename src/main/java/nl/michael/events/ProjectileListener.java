package nl.michael.events;

import org.spongepowered.api.entity.projectile.arrow.Arrow;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.CollideBlockEvent;
import org.spongepowered.api.event.filter.cause.First;

public class ProjectileListener {

    @Listener
    public void onProjectileCollide(CollideBlockEvent event, @First Arrow projectile) {
        if (projectile != null) {
            projectile.remove();
        }
    }
}
