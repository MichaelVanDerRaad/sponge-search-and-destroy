package nl.michael.events;

import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.entity.SpawnEntityEvent;


public class SpawnEntityListener implements EventListener<SpawnEntityEvent.Spawner> {

    @Override
    public void handle(SpawnEntityEvent.Spawner event) throws Exception {
        event.setCancelled(true);
    }
}
