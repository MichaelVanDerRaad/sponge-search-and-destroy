package nl.michael.events;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.item.inventory.DropItemEvent;

public class ItemDropListener implements EventListener<DropItemEvent.Pre> {
    @Listener
    public void handle(DropItemEvent.Pre event) {
        if (event.getSource() instanceof Player) {
            event.setCancelled(true);
        }
    }
}
