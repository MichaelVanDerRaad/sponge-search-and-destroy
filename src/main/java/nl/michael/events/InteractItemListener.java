package nl.michael.events;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.item.inventory.InteractItemEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;

public class InteractItemListener {

    @Listener
    public void onItemInteractPrimary(InteractItemEvent event) {
        if((event.getSource() instanceof Player)) {
            Player player = (Player) event.getSource();

            ItemStackSnapshot itemStackSnapshot = event.getItemStack();
            ItemType itemType = itemStackSnapshot.getType();

            if(itemType == ItemTypes.FLINT_AND_STEEL) {
                event.setCancelled(true);

                player.sendMessage(Text.of("FIre!!"));
            } else {
                event.setCancelled(true);
            }
        } else {
            event.setCancelled(true);
        }
    }
}
