package nl.michael.events;

import com.flowpowered.math.vector.Vector3i;
import nl.michael.plugin.SearchAndDestroy;
import nl.michael.teams.ITeam;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.text.Text;

public class BlockPlaceListener implements EventListener<ChangeBlockEvent.Place> {

    private ITeam team;

    public BlockPlaceListener(ITeam team) {
        this.team = team;
    }

    @Override
    public void handle(ChangeBlockEvent.Place event) throws Exception {
        if(!(event.getSource() instanceof Player)) return;

        Player player = (Player) event.getSource();

        BlockSnapshot placedBlockSnapshot = event.getTransactions().get(0).getFinal();
        BlockType blockType = placedBlockSnapshot.getState().getType();

        if(blockType == BlockTypes.TNT) {
            Vector3i blockPosition = placedBlockSnapshot.getPosition();
            team.setBombPosition(blockPosition);

            player.sendMessage(Text.of("Bomb configured for team " + team.getType().toString()));
            player.getInventory().clear();

            Sponge.getEventManager().unregisterListeners(this);
            Sponge.getEventManager().registerListeners(SearchAndDestroy.getPlugin(), new BlockInteractionListener(team.getType(), blockPosition));
        }
    }
}
