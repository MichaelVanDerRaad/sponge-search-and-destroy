package nl.michael.events;

import nl.michael.plugin.MatchManager;
import nl.michael.plugin.PlayerData;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.cause.entity.damage.source.EntityDamageSource;
import org.spongepowered.api.event.cause.entity.damage.source.IndirectEntityDamageSource;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.filter.cause.All;

public class PlayerAttackListener {

    @Listener
    public void onPlayerAttack(DamageEntityEvent event, @All(ignoreEmpty=false) EntityDamageSource[] sources) {
        if(event.getTargetEntity().getType() != EntityTypes.PLAYER) return;

        if(!MatchManager.getInstance().isCombatEnabled()) {
            event.setCancelled(true);
            return;
        }

        Player target = (Player) event.getTargetEntity();
        Player attacker = null;
        for (int i = 0; i < sources.length; i++) {
            if (sources[i].getSource().getType() == EntityTypes.PLAYER) {
                attacker = (Player)sources[i].getSource();
                break;
            } else if ((sources[i] instanceof IndirectEntityDamageSource)) {
                Entity indirectSrc = ((IndirectEntityDamageSource) sources[i]).getIndirectSource();
                if (indirectSrc.getType() == EntityTypes.PLAYER) {
                    attacker = (Player)indirectSrc;
                    break;
                }
            }
        }

        if(attacker != null) {
            PlayerData targetData = MatchManager.getInstance().getPlayerData(target.getUniqueId());
            PlayerData attackerData = MatchManager.getInstance().getPlayerData(attacker.getUniqueId());

            if(targetData.isInTeamWith(attackerData)) {
                event.setCancelled(true);
            }
        }
    }
}
