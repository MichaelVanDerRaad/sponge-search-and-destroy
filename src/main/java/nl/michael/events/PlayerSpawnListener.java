package nl.michael.events;

import com.flowpowered.math.vector.Vector3i;
import nl.michael.plugin.MatchManager;
import nl.michael.plugin.PlayerData;
import nl.michael.services.ConfigService;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.living.humanoid.player.RespawnPlayerEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;

public class PlayerSpawnListener {
    Task.Builder taskBuilder = Task.builder();

    @Listener
    public void onPlayerJoin(ClientConnectionEvent event) {
        if(event.getCause().root() instanceof Player) {
            Player player = (Player) event.getCause().root();
            player.getInventory().clear();
            player.offer(player.getGameModeData().set(Keys.GAME_MODE, GameModes.ADVENTURE));
            player.offer(Keys.EXPERIENCE_LEVEL, 0);
            player.getFoodData().exhaustion().set(0.0);
            player.getFoodData().saturation().set(0.0);

            Vector3i newPosition = ConfigService.getInstance().getHubPosition();
            if(newPosition != null) {
                player.setTransform(player.getTransform().setPosition(ConfigService.getInstance().getHubPosition().toDouble()));
                player.sendMessage(Text.of("Welcome to search and destroy!"));
            }
        }
    }

    @Listener
    public void onPlayerRespawn(RespawnPlayerEvent event) {
        if(event.getCause().root() instanceof Player) {
            Player respawnedPlayerInstance = (Player) event.getCause().root();
            PlayerData playerData = MatchManager.getInstance().getPlayerData(respawnedPlayerInstance.getUniqueId());
            // Set new player instance
            playerData.setPlayer(respawnedPlayerInstance);
            playerData.setAlive(false);
            event.getOriginalPlayer().offer(Keys.GAME_MODE, GameModes.SPECTATOR);

            respawnedPlayerInstance.sendMessage(Text.of("You are now a spectator! You will respawn in 10 seconds"));
            respawnedPlayerInstance.offer(Keys.EXPERIENCE_LEVEL, 0);
            event.setToTransform(event.getToTransform().setPosition(event.getFromTransform().getPosition()));

            MatchManager.getInstance().onPlayerRespawn(playerData);
        }
    }
}