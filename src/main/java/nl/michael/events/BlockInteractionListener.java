package nl.michael.events;

import com.flowpowered.math.vector.Vector3i;
import nl.michael.plugin.MatchManager;
import nl.michael.teams.ITeam;
import nl.michael.teams.Team;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;

import java.util.Timer;
import java.util.TimerTask;

public class BlockInteractionListener {
    private double lastBombFuse = 0.0;
    private Team teamType;
    private Vector3i bombLocation;
    private Timer fuseTimer;

    public BlockInteractionListener(Team teamType, Vector3i bombLocation) {
        this.teamType = teamType;
        this.bombLocation = bombLocation;
    }

    @Listener
    public void onBlockInteractSecondary(InteractBlockEvent.Secondary event) {
        if(!(event.getSource() instanceof Player)) return;

        Player player = (Player) event.getSource();

        ItemStack itemStack = player.getItemInHand(HandTypes.MAIN_HAND).get();
        ItemType itemType = itemStack.getType();

        if(itemType == ItemTypes.FLINT_AND_STEEL) {
            BlockSnapshot targetBlock = event.getTargetBlock();

            if(targetBlock.getState().getType() == BlockTypes.TNT) {
                if(!bombLocation.equals(targetBlock.getPosition())) return;

                if(!MatchManager.getInstance().isCombatEnabled()) {
                    event.setCancelled(true);
                    return;
                }


                ITeam playerTeam = MatchManager.getInstance().getPlayerData(player.getUniqueId()).getTeam();

                if(teamType.equals(playerTeam.getType())) {
                    player.sendMessage(Text.of("This is your own bomb"));
                    event.setCancelled(true);
                    return;
                }

                if(fuseTimer != null) fuseTimer.cancel();

                lastBombFuse = MatchManager.getInstance().fuseBomb(targetBlock.getPosition(), MatchManager.getInstance().getPlayerData(player.getUniqueId()).getFusePower());
                player.getWorld().playSound(SoundTypes.BLOCK_FIRE_EXTINGUISH, player.getPosition(), 1.0);

                fuseTimer = new Timer();
                fuseTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        MatchManager.getInstance().resetBombFuse(targetBlock.getPosition());
                        player.offer(Keys.TOTAL_EXPERIENCE, 0);
                    }
                }, 400);

                player.offer(Keys.TOTAL_EXPERIENCE, ((lastBombFuse / 18) <= 18) ? (int) Math.round(lastBombFuse / 18) : 18);

                if (lastBombFuse > 100) {
                    player.sendMessage(Text.of("Bomb is activated"));
                    player.getWorld().createEntity(EntityTypes.PRIMED_TNT, targetBlock.getPosition());
                    targetBlock.getLocation().get().removeBlock();
                    MatchManager.getInstance().decideWinner();

                    return;
                }

                event.setCancelled(true);
            } else {
                // Always cancel flint and steel events if its not tnt
                event.setCancelled(true);
            }
        }
    }

    @Listener
    public void onBlockInteractPrimary(InteractBlockEvent.Primary event) {
        if(!(event.getSource() instanceof Player)) return;

        Player player = (Player) event.getSource();

        if(!player.get(Keys.GAME_MODE).get().equals(GameModes.CREATIVE)) event.setCancelled(true);
    }
}
