package nl.michael.events;

import nl.michael.plugin.MatchManager;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.event.EventListener;
import org.spongepowered.api.event.world.ExplosionEvent;
import org.spongepowered.api.world.explosion.Explosion;

public class ExplosionListener implements EventListener<ExplosionEvent.Detonate> {
    @Override
    public void handle(ExplosionEvent.Detonate event) throws Exception {
        event.setCancelled(true);
        Explosion explosion = (Explosion) event.getSource();
        explosion.getLocation().setBlock(BlockState.builder().blockType(BlockTypes.TNT).build());

        MatchManager.getInstance().stopMatch();
    }
}