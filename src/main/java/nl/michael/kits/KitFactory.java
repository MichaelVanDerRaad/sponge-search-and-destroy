package nl.michael.kits;

public class KitFactory {
    public static IKit getKitInstance(KitType kitType)
    {
        switch (kitType) {
            case WARRIOR:
                return new WarriorKit();
            case ARCHER:
                return new ArcherKit();
            case GHOST:
                return new GhostKit();
            default:
                return null;
        }
    }

    public static IKit getDefaultKit() {
        return new WarriorKit();
    }
}
