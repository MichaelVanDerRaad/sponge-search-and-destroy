package nl.michael.kits;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;

import java.util.LinkedList;
import java.util.List;

public class GhostKit implements IKit {

    @Override
    public void applyTo(Player player) {
        player.getInventory().clear();

        ItemStack sword = ItemStack.of(ItemTypes.GOLDEN_SWORD, 1);
        sword.offer(Keys.UNBREAKABLE, true);

        List<PotionEffect> effects = new LinkedList<PotionEffect>();
        PotionEffect invisiblePotion = PotionEffect.builder().potionType(PotionEffectTypes.INVISIBILITY).duration(1000000).build();
        effects.add(invisiblePotion);

        player.setItemInHand(HandTypes.MAIN_HAND, sword);
        player.offer(Keys.POTION_EFFECTS, effects);

        ItemStack flintAndSteel = ItemStack.of(ItemTypes.FLINT_AND_STEEL, 1);
        flintAndSteel.offer(Keys.DISPLAY_NAME, Text.of("Bomb detonator"));
        flintAndSteel.offer(Keys.UNBREAKABLE, true);
        player.getInventory().root().offer(flintAndSteel);
    }

    @Override
    public KitType getType() {
        return KitType.GHOST;
    }
}
