package nl.michael.kits;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;

public class ArcherKit implements IKit {
    @Override
    public void applyTo(Player player) {
        player.getInventory().clear();

        ItemStack bow = ItemStack.of(ItemTypes.BOW, 1);
        ItemStack helmet = ItemStack.of(ItemTypes.LEATHER_HELMET, 1);
        ItemStack chestplate = ItemStack.of(ItemTypes.LEATHER_CHESTPLATE, 1);
        ItemStack leggings = ItemStack.of(ItemTypes.LEATHER_LEGGINGS, 1);
        ItemStack arrows = ItemStack.of(ItemTypes.ARROW, 24);

        bow.offer(Keys.UNBREAKABLE, true);
        helmet.offer(Keys.UNBREAKABLE, true);
        chestplate.offer(Keys.UNBREAKABLE, true);
        leggings.offer(Keys.UNBREAKABLE, true);

        player.setItemInHand(HandTypes.MAIN_HAND, bow);
        player.setHelmet(helmet);
        player.setChestplate(chestplate);
        player.setLeggings(leggings);
        player.getInventory().offer(arrows);

        ItemStack flintAndSteel = ItemStack.of(ItemTypes.FLINT_AND_STEEL, 1);
        flintAndSteel.offer(Keys.DISPLAY_NAME, Text.of("Bomb detonator"));
        flintAndSteel.offer(Keys.UNBREAKABLE, true);
        player.getInventory().root().offer(flintAndSteel);
    }

    @Override
    public KitType getType() {
        return KitType.ARCHER;
    }
}
