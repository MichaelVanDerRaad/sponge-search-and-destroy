package nl.michael.kits;

public enum KitType {
    WARRIOR,
    ARCHER,
    GHOST
}
