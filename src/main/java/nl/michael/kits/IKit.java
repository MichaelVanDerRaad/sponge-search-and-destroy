package nl.michael.kits;

import org.spongepowered.api.entity.living.player.Player;

public interface IKit {
    void applyTo(Player player);

    KitType getType();
}