package nl.michael.kits;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;

public class WarriorKit implements IKit {
    @Override
    public void applyTo(Player player) {
        player.getInventory().clear();

        ItemStack sword = ItemStack.of(ItemTypes.IRON_SWORD, 1);
        ItemStack helmet = ItemStack.of(ItemTypes.LEATHER_HELMET, 1);
        ItemStack chestplate = ItemStack.of(ItemTypes.IRON_CHESTPLATE, 1);
        ItemStack leggings = ItemStack.of(ItemTypes.IRON_LEGGINGS, 1);

        sword.offer(Keys.UNBREAKABLE, true);
        helmet.offer(Keys.UNBREAKABLE, true);
        chestplate.offer(Keys.UNBREAKABLE, true);
        leggings.offer(Keys.UNBREAKABLE, true);

        player.setItemInHand(HandTypes.MAIN_HAND, sword);
        player.setHelmet(helmet);
        player.setChestplate(chestplate);
        player.setLeggings(leggings);

        ItemStack flintAndSteel = ItemStack.of(ItemTypes.FLINT_AND_STEEL, 1);
        flintAndSteel.offer(Keys.DISPLAY_NAME, Text.of("Bomb detonator"));
        flintAndSteel.offer(Keys.UNBREAKABLE, true);
        player.getInventory().root().offer(flintAndSteel);
    }

    @Override
    public KitType getType() {
        return KitType.WARRIOR;
    }
}
