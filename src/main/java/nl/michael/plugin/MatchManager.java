package nl.michael.plugin;

import com.flowpowered.math.vector.Vector3i;
import nl.michael.bomb.BombDetonateEvent;
import nl.michael.bomb.BombDetonateEventHandler;
import nl.michael.bomb.TeamBomb;
import nl.michael.services.ConfigService;
import nl.michael.tasks.DelayedRearm;
import nl.michael.teams.ITeam;
import nl.michael.teams.Team;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.MessageChannel;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class MatchManager {
    private static MatchManager instance;

    private Map<String, PlayerData> players;
    private Map<String, PlayerData> playerResetList;
    private Map<String, DelayedRearm> rearmConsumers;

    private Map<Vector3i, TeamBomb> bombs;
    private boolean gameStarted;
    private boolean combatEnabled;

    private MatchManager() {
        players = new HashMap<String, PlayerData>();
        playerResetList = new HashMap<String, PlayerData>();
        bombs = new HashMap<Vector3i, TeamBomb>();
        rearmConsumers = new HashMap<String, DelayedRearm>();
    }

    public Text addPlayerToTeam(Player player, ITeam team) {
        PlayerData playerData = new PlayerData(player, team);
        players.put(player.getIdentifier(), playerData);

        return Text.of("You are now in team " + playerData.getTeam().toString());
    }

    public void resetParticipatingPlayers() {
        for (Map.Entry<String, PlayerData> playerDataEntry:
                players.entrySet()) {
            PlayerData playerData = playerDataEntry.getValue();
            if (playerData.playerIsSpawned()) {
                SearchAndDestroy.getPlugin().getLogger().info("Reset spawned player");
                playerData.movePlayerToHub();
            } else {
                SearchAndDestroy.getPlugin().getLogger().info("Player not spawned yet, adding to list");
                playerResetList.put(playerData.getPlayer().getIdentifier(), playerData);
            }
        }
    }

    private void initializePlayers() {
        for(Map.Entry<String, PlayerData> playerDataEntry: players.entrySet()) {
            Player player = playerDataEntry.getValue().getPlayer();
            PlayerData playerData = MatchManager.getInstance().getPlayerData(player.getUniqueId());
            playerData.onMatchStart();
        }
    }

    public void removePlayerFromMatch(Player player) {
        if(players.containsKey(player.getIdentifier())) players.remove(player.getIdentifier());
    }

    public double fuseBomb(Vector3i position, double energy) {
        if(bombs.containsKey(position)) {
            TeamBomb bombState = bombs.get(position);
            bombState.setFuseState(bombState.getFuseState() + energy);

            return bombState.getFuseState();
        }

        return 0.0;
    }

    public void resetBombFuse(Vector3i position) {
        if(bombs.containsKey(position)) {
            TeamBomb bombState = bombs.get(position);
            bombState.setFuseState(0);
        }
    }

    public PlayerData getPlayerData(UUID playerId) {
        PlayerData playerDataToReset = playerResetList.getOrDefault(playerId.toString(), null);

        if (playerDataToReset != null) {
            SearchAndDestroy.getPlugin().getLogger().info("Player found to reset object: " + playerDataToReset.toString());
        }

        PlayerData playerData = players.getOrDefault(
                playerId.toString(),
                playerDataToReset
        );

        return playerData;
    }

    public void startMatch() {
        bombs.put(ConfigService.getInstance().getRedBombPosition(), new TeamBomb(Team.RED));
        bombs.put(ConfigService.getInstance().getBlueBombPosition(), new TeamBomb(Team.BLUE));

        initializePlayers();

        gameStarted = true;
        combatEnabled = true;
    }

    public void decideWinner() {
        combatEnabled = false;

        TeamBomb winner = null;
        for (Map.Entry<Vector3i, TeamBomb> entry: bombs.entrySet()) {
            if(winner == null) winner = entry.getValue();
            else if(entry.getValue().getFuseState() < winner.getFuseState()) winner = entry.getValue();
        }

        MessageChannel.TO_ALL.send(Text.of("The winning team is team " + winner.getTeam().getLabel()));
    }

    public void stopMatch() {
        gameStarted = false;
        combatEnabled = false;

        resetParticipatingPlayers();
        bombs.clear();
        players.clear();
    }

    // value <= 2/3 or value >= 1/3 is a balanced ratio
    public float calculateTeamBalance()
    {
        int redPlayers = 0;
        int bluePlayers = 0;
        for (Map.Entry<String, PlayerData> entry : players.entrySet()) {
            PlayerData playerData = entry.getValue();
            if (playerData.getTeam().getType() == Team.RED) {
                redPlayers++;
            } else if (playerData.getTeam().getType() == Team.BLUE) {
                bluePlayers++;
            }
        }

        if (redPlayers == 0 && bluePlayers == 1 || redPlayers == 1 && bluePlayers == 0) {
            return 0.5f;
        } else {
            return ((float)redPlayers) / (redPlayers + bluePlayers);
        }
    }

    public void onPlayerRearm(PlayerData playerData) {
        rearmConsumers.remove(playerData.getPlayer().getIdentifier());
    }

    public void onPlayerRespawn(PlayerData playerData) {
        // Player respawned after death
        if (playerResetList.containsKey(playerData.getPlayer().getIdentifier())) {
            // Player should be moved to hub, last match has been stopped
            Task.builder().execute(new Runnable() {
                @Override
                public void run() {
                    SearchAndDestroy.getPlugin().getLogger().info("Teleport to hub");
                    playerData.movePlayerToHub();
                    playerResetList.remove(playerData.getPlayer().getIdentifier());

                }
            })
                    .delay(1, TimeUnit.SECONDS)
                    .submit(SearchAndDestroy.getPlugin());
        } else {
            DelayedRearm rearmConsumer = new DelayedRearm(playerData);
            Task.builder().execute(rearmConsumer)
                    .interval(1, TimeUnit.SECONDS)
                    .submit(SearchAndDestroy.getPlugin());

            rearmConsumers.put(playerData.getPlayer().getIdentifier(), rearmConsumer);
        }
    }

    public boolean isGameStarted() {
        return gameStarted;
    }

    public boolean isCombatEnabled() {
        return combatEnabled;
    }

    public boolean shouldMovePlayerToHub(PlayerData playerData) {
        return (playerResetList.containsKey(playerData.getPlayer().getIdentifier()));
    }

    public static MatchManager getInstance() {
        if(instance == null) instance = new MatchManager();

        return instance;
    }
}
