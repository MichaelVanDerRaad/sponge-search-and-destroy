package nl.michael.plugin;

import com.flowpowered.math.vector.Vector3i;
import com.google.inject.Inject;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import nl.michael.command.*;
import nl.michael.events.*;
import nl.michael.services.ConfigService;
import nl.michael.teams.Team;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.SpawnEntityEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.event.world.ExplosionEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.permission.PermissionService;

import java.nio.file.Path;

@Plugin(
        id = "searchanddestroy",
        name = "SearchAndDestroy",
        version = "1.0"
)
public class SearchAndDestroy {
    private static SearchAndDestroy plugin;

    private PermissionService permissionService;

    @Inject
    private Logger logger;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private Path defaultConfig;

    @Inject
    @DefaultConfig(sharedRoot = true)
    private ConfigurationLoader<CommentedConfigurationNode> configManager;

    @Inject
    @ConfigDir(sharedRoot = false)
    private Path privateConfigDir;

    public SearchAndDestroy() {
        // TODO: Create event manager for managing registered events

        if(plugin == null) {
            plugin = this;
        }
    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        ConfigurationLoader<CommentedConfigurationNode> configFileLoader = HoconConfigurationLoader.builder().setPath(defaultConfig).build();

        ConfigService.instantiate(configFileLoader);

        permissionService = Sponge.getServiceManager().provideUnchecked(PermissionService.class);

        registerCommands();

        registerEvents();

        logger.info("Plugin initialized!");
    }

    private void registerCommands() {
        BombConfigCommand bombConfigCommand = new BombConfigCommand(plugin);
        bombConfigCommand.registerCommand();
        bombConfigCommand.registerPermissions(permissionService);

        HubConfigCommand hubConfigCommand = new HubConfigCommand(plugin);
        hubConfigCommand.registerCommand();
        hubConfigCommand.registerPermissions(permissionService);

        TeamPositionCommand teamPositionCommand = new TeamPositionCommand(plugin);
        teamPositionCommand.registerCommand();
        teamPositionCommand.registerPermissions(permissionService);

        MatchCommand matchCommand = new MatchCommand(plugin);
        matchCommand.registerCommand();
        matchCommand.registerPermissions(permissionService);

        TeamCommand teamCommand = new TeamCommand(plugin);
        teamCommand.registerCommand();
        //teamCommand.registerPermissions(permissionService);

        KitCommand kitCommand = new KitCommand(plugin);
        kitCommand.registerCommand();
    }

    private void registerEvents() {
        // TODO: Refactor to event listeners
        Sponge.getEventManager().registerListener(this, DropItemEvent.Pre.class, new ItemDropListener());
        Sponge.getEventManager().registerListener(this, SpawnEntityEvent.Spawner.class, new SpawnEntityListener());
        Sponge.getEventManager().registerListener(this, ExplosionEvent.Detonate.class, new ExplosionListener());

        Sponge.getEventManager().registerListeners(this, new PlayerAttackListener());
        Sponge.getEventManager().registerListeners(this, new ProjectileListener());
        Sponge.getEventManager().registerListeners(this, new PlayerSpawnListener());

        Vector3i blueBombPosition = ConfigService.getInstance().getBlueBombPosition();
        Vector3i redBombPosition = ConfigService.getInstance().getRedBombPosition();
        if(blueBombPosition != null) Sponge.getEventManager().registerListeners(this, new BlockInteractionListener(Team.BLUE, blueBombPosition));
        if(redBombPosition != null) Sponge.getEventManager().registerListeners(this, new BlockInteractionListener(Team.RED, redBombPosition));
    }

    public Logger getLogger() { return logger; }

    public static SearchAndDestroy getPlugin() {
        return plugin;
    }
}