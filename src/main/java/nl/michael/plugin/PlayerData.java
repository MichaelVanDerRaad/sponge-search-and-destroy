package nl.michael.plugin;

import nl.michael.kits.IKit;
import nl.michael.kits.KitFactory;
import nl.michael.services.ConfigService;
import nl.michael.teams.ITeam;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.text.Text;

import java.util.ArrayList;

public class PlayerData {
    private Player player;
    private ITeam team;
    private IKit kit;
    private double fusePower;
    private boolean alive = true;
    private boolean moveToHubWhenSpawned = false;

    public PlayerData(Player player, ITeam team) {
        this.player = player;
        this.team = team;
        this.fusePower = 5.5;
    }

    public Player getPlayer() {
        return player;
    }

    public ITeam getTeam() {
        return team;
    }

    public double getFusePower() {
        return fusePower;
    }

    public void movePlayerToHub() {
        clearStats();
        player.setLocationSafely(player.getLocation().setBlockPosition(ConfigService.getInstance().getHubPosition()));
    }

    public void rearm() {
        team.teleportPlayer(player);
        alive = true;
        clearStats();
        if (kit != null) {
            kit.applyTo(player);
        }
    }

    public void clearStats() {
        player.offer(Keys.HEALTH, player.getHealthData().maxHealth().get());
        player.offer(Keys.FOOD_LEVEL, player.getFoodData().foodLevel().getMaxValue());
        player.offer(Keys.GAME_MODE, GameModes.ADVENTURE);
        player.offer(Keys.POTION_EFFECTS, new ArrayList<PotionEffect>());

        player.getInventory().clear();
    }

    public void onMatchStart() {
        clearStats();

        if(kit != null) {
            kit.applyTo(player);
        } else {
            IKit defaultKit = KitFactory.getDefaultKit();
            defaultKit.applyTo(player);
            this.setKit(defaultKit);
            player.sendMessage(Text.of("No kit selected, using default kit"));
        }

        team.teleportPlayer(player);
        player.sendMessage(Text.of("Match started, detonate your opponent's bomb to destroy their base"));
    }

    public boolean isInTeamWith(PlayerData otherPlayerData)
    {
        if (this.team == null || otherPlayerData.getTeam() == null) {
            return false;
        } else {
            return (this.team.getClass().equals(otherPlayerData.getTeam().getClass()));
        }
    }

    public void setKit(IKit kit) { this.kit = kit; }

    public IKit getKit() {
        return kit;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public boolean isAlive() {
        return this.alive;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean playerIsSpawned() {
        return (player != null && player.get(Keys.HEALTH).get() != 0);
    }
}
