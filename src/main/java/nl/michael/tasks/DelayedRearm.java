package nl.michael.tasks;

import nl.michael.plugin.MatchManager;
import nl.michael.plugin.PlayerData;
import nl.michael.plugin.SearchAndDestroy;
import org.spongepowered.api.scheduler.Task;

import java.util.function.Consumer;

public class DelayedRearm implements Consumer<Task> {
    int count = 10;
    PlayerData playerData;

    public DelayedRearm(PlayerData playerData) {
        this.playerData = playerData;
    }

    @Override
    public void accept(Task task) {
        if (count == 0) {
            SearchAndDestroy.getPlugin().getLogger().info("Respawn!");
            playerData.rearm();
            MatchManager.getInstance().onPlayerRearm(playerData);
            task.cancel();
        } else {
            SearchAndDestroy.getPlugin().getLogger().info("Respawn " + playerData.getPlayer().getDisplayNameData().displayName().get().toPlain() + " in " + count);
        }

        count--;
    }
}
